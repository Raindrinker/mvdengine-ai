#include "CarController.h"

lm::mat4 spos;

void CarController::init() {

	transform = &ECS.getComponentFromEntity<Transform>(owner_);

	spos = transform->getGlobalMatrix(ECS.getAllComponents<Transform>());

}

void CarController::update(float dt) {

	int player = ECS.getEntity("PlayerFree");
	lm::vec3 player_pos = ECS.getComponentFromEntity<Transform>(player).position();
	lm::vec3 player_pos_floor = lm::vec3(player_pos.x, 0, player_pos.z);

	Collider& collider_front = ECS.getComponentFromEntity<Collider>(owner_);
	Collider& collider_left = ECS.getAllComponents<Collider>()[collider_left_id];
	Collider& collider_right = ECS.getAllComponents<Collider>()[collider_right_id];

	std::vector<Transform> vec;

	lm::vec3 direction = transform->getGlobalMatrix(vec).front();
	lm::vec3 direction_to_player = (player_pos_floor - transform->position()).normalize();

	float dot = direction.x*direction_to_player.x + direction.z*direction_to_player.z;
	float det = direction.x*direction_to_player.z - direction.z*direction_to_player.x;
	float angle = atan2(det, dot);

	std::cout << angle << std::endl;

	if (collider_left.colliding) {
		transform->rotateLocal(dt, lm::vec3(0, 1, 0));
	}else if (collider_right.colliding) {
		
		transform->rotateLocal(-dt, lm::vec3(0, 1, 0));
	}
	else {
		if (angle > 0) {
			transform->rotateLocal(dt, lm::vec3(0, 1, 0));
		}
		else {
			transform->rotateLocal(-dt, lm::vec3(0, 1, 0));
		}
	}

	if (!collider_front.colliding) {
		transform->translateLocal(0, 0, dt*move_speed);
	}

	

}

void CarController::setLeftCollider(int col) {

	collider_left_id = col;

}

void CarController::setRightCollider(int col) {

	collider_right_id = col;

}