#pragma once
#include "includes.h"
#include "ScriptSystem.h"
#include "extern.h"
#include "Components.h"

class CarController : public Script {

public:
	CarController(int owner) : Script(owner) {};

	void init();

	void update(float dt);

	Transform* transform;
	int collider_front_id;
	int collider_left_id;
	int collider_right_id;

	void setLeftCollider(int col);
	void setRightCollider(int col);

	float move_speed = 5.0f;
	float turn_speed = 1.0f;
	
};